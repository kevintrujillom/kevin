<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TrainerFactory
 *
 * @author Kev
 */
class TrainerFactory {
    
    public static function getTrainer($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \Trainer {
        return new Trainer($id, $nombre, $medallas, $edad, $pueblo, $pokemons);
    }
//    public static function getTrainerAmistoso($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \TrainerAmistoso {
//        return new TrainerAmistoso($id, $nombre, $medallas, $edad, $pueblo, $pokemons);
//    }
//    public static function getTrainerAventajado($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \TrainerAventajado {
//        return new TrainerAventajado($id, $nombre, $medallas, $edad, $pueblo, $pokemons);
//    }
//    public static function getTrainerCompetitivo($nombre, $medallas, $edad, $pueblo,$pokemons): \TrainerCompetitivo {
//        return new TrainerCompetitivo($id, $nombre, $medallas, $edad, $pueblo, $pokemons);
//    }
}
