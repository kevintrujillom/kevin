<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoFactory
 *
 * @author Kev
 */
class TipoFactory implements ITipoFactory {
    
    public static function getTipo($tipo, $fortalezas, $debilidades): \Tipo {
        return new Tipo($tipo, $fortalezas, $debilidades);
    }
    
}
