<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PokemonFactory
 *
 * @author Kev
 */
class PokemonFactory implements IPokemonFactory{
    
    public static function getPokemon($id, $nombre, $genero, $hp, $tipo, $ataques, $nivel): \Pokemon {
        return new Pokemon($id, $nombre, $genero, $hp, $tipo, $ataques, $nivel);
    }
    
}
