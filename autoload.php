<?php

require 'config.php';
    

spl_autoload_register(function($class){
   
   if(file_exists(BRIDGES.$class.".php")){
           include "bridges/".$class.".php";
   }
    if(file_exists(MODELS.$class.".php")){
       include "models/".$class.".php";
   }
   
   if(file_exists(LIBS.$class.".php")){
           include "libs/".$class.".php";
   }
   
   if(file_exists(INTERFACES.$class.".php")){
           include "interfaces/".$class.".php";
   }
   
   if(file_exists(FACTORIES.$class.".php")){
           include "factories/".$class.".php";
   }
   
});