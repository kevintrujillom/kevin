<?php

require './autoload.php';

class AtaqueTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testAtaque(){
        /*
         * INSTANCIAS DE TIPOS
         */
        $debilidadFuego = array('debilidad1' => 'Agua',
            'debilidad2' => 'Tierra',
            'debilidad3' => 'Roca'
        );
        $fortalezaFuego = array('fortaleza1' => 'Hierba',
            'fortaleza2' => 'Bicho',
            'fortaleza3' => 'Hielo'
        );
        $tipoFuego = TipoFactory::getTipo("Fuego", $fortalezaFuego, $debilidadFuego);


        $debilidadHierba = array('debilidad1' => 'Fuego',
            'debilidad2' => 'Bicho',
            'debilidad3' => 'Hielo'
        );
        $fortalezaHierba = array('fortaleza1' => 'Agua',
            'fortaleza2' => 'Tierra',
            'fortaleza3' => 'Roca'
        );
        $tipoHierba = TipoFactory::getTipo("Hierba", $fortalezaHierba, $debilidadHierba);

        /*
         * INSTANCIAS DE ATAQUES
         */
        $ataquesFuego = array('lanzallamas' => AtaqueFactory::getAtaque("Lanzallamas", $tipoFuego, 90, 15),
            'punodefuego' => AtaqueFactory::getAtaque("Puño de fuego", $tipoFuego, 80, 10),
            'humareda' => AtaqueFactory::getAtaque("Humareda", $tipoFuego, 80, 15),
            'girofuego' => AtaqueFactory::getAtaque("Giro fuego", $tipoFuego, 35, 20)
        );
        $ataquesHierba = array('gigadrenado' => AtaqueFactory::getAtaque("Gigadrenado", $tipoHierba, 75, 15),
            'energibola' => AtaqueFactory::getAtaque("Energibola", $tipoHierba, 90, 10),
            'hojaafilada' => AtaqueFactory::getAtaque("Hoja afilada", $tipoHierba, 55, 20),
            'patadatropical' => AtaqueFactory::getAtaque("Patada tropical", $tipoHierba, 75, 10)
        );

        /*
         * INSTANCIAS DE POKEMONES
         */
        $charizard = new Pokemon(1, "Charizard", "macho", 500, $tipoFuego, $ataquesFuego, 100);
        $venusaur = new Pokemon(4, "Venusaur", "macho", 500, $tipoHierba, $ataquesHierba, 100);

        /*
         * ESTA PRUBA VERIFICA SI A UN POKEMON
         * SE LE RESTAN LOS PPS DESPUES DE UN
         * ATAQUE
         */
        $atacante2 = $charizard;
        $defensor2 = $venusaur;

        function cantidadAtaques($atacante2, $defensor2) {
            if ($atacante2->getTipo()->getTipo() == "Fuego") {

                if ($defensor2->getTipo()->getTipo() == "Hierba") {        
                    $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
                    $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
                    return $atacante2->getAtaques()[lanzallamas]->getPp();            
                }
                if ($defensor2->getTipo()->getTipo() == "Fuego") {
                    $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
                    $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
                    return $atacante2->getAtaques()[lanzallamas]->getPp(); 
                }
                if ($defensor2->getTipo()->getTipo() == "Agua") {
                    $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
                    $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
                    return $atacante2->getAtaques()[lanzallamas]->getPp(); 
                }
                if ($defensor2->getTipo()->getTipo() != "Fuego" || $defensor2->getTipo()->getTipo() != "Agua" || $defensor2->getTipo()->getTipo() != "Hierba") {
                    $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
                    $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
                    return $atacante2->getAtaques()[lanzallamas]->getPp(); 
                }
            }

            $this->assertNotEquals($atacante2->getAtaques()[lanzallamas]->getPp(), 15);
        }

    }
}