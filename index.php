<?php

require 'autoload.php';

/*
 * INSTANCIAS DE TIPOS
 */
$debilidadFuego = array('debilidad1' => 'Agua',
    'debilidad2' => 'Tierra',
    'debilidad3' => 'Roca'
);
$fortalezaFuego = array('fortaleza1' => 'Hierba',
    'fortaleza2' => 'Bicho',
    'fortaleza3' => 'Hielo'
);

//$tipoFuego = new Tipo("Fuego", $fortalezaFuego, $debilidadFuego);
$tipoFuego = TipoFactory::getTipo("Fuego", $fortalezaFuego, $debilidadFuego);

$debilidadAgua = array('debilidad1' => 'Electrico',
    'debilidad2' => 'Planta'
);
$fortalezaAgua = array('fortaleza1' => 'Fuego',
    'fortaleza2' => 'Tierra',
    'fortaleza3' => 'Roca'
);

//$tipoAgua = new Tipo("Agua", $fortalezaAgua, $debilidadAgua);
$tipoAgua = TipoFactory::getTipo("Agua", $fortalezaAgua, $debilidadAgua);

$debilidadHierba = array('debilidad1' => 'Fuego',
    'debilidad2' => 'Bicho',
    'debilidad3' => 'Hielo'
);
$fortalezaHierba = array('fortaleza1' => 'Agua',
    'fortaleza2' => 'Tierra',
    'fortaleza3' => 'Roca'
);

//$tipoHierba = new Tipo("Hierba", $fortalezaHierba, $debilidadHierba);
$tipoHierba = TipoFactory::getTipo("Hierba", $fortalezaHierba, $debilidadHierba);

//$tipoFantasma = new Tipo("Fantasma", $fortalezaHierba, $debilidadHierba);
$tipoFantasma = TipoFactory::getTipo("Hierba", $fortalezaHierba, $debilidadHierba);

/*
 * INSTANCIAS DE ATAQUES
 */
$ataquesFuego = array('lanzallamas' => AtaqueFactory::getAtaque("Lanzallamas", $tipoFuego, 90, 15),
    'punodefuego' => AtaqueFactory::getAtaque("Puño de fuego", $tipoFuego, 80, 10),
    'humareda' => AtaqueFactory::getAtaque("Humareda", $tipoFuego, 80, 15),
    'girofuego' => AtaqueFactory::getAtaque("Giro fuego", $tipoFuego, 35, 20)
);

$ataquesAgua = array('pistoladeagua' => AtaqueFactory::getAtaque("Pistola de agua", $tipoAgua, 60, 15),
    'canondeagua' => AtaqueFactory::getAtaque("Cañon de agua", $tipoAgua, 90, 10),
    'acuajet' => AtaqueFactory::getAtaque("Acua jet", $tipoAgua, 40, 20),
    'hidrobomba' => AtaqueFactory::getAtaque("Giro fuego", $tipoAgua, 110, 5)
);

$ataquesHierba = array('gigadrenado' => AtaqueFactory::getAtaque("Gigadrenado", $tipoHierba, 75, 15),
    'energibola' => AtaqueFactory::getAtaque("Energibola", $tipoHierba, 90, 10),
    'hojaafilada' => AtaqueFactory::getAtaque("Hoja afilada", $tipoHierba, 55, 20),
    'patadatropical' => AtaqueFactory::getAtaque("Patada tropical", $tipoHierba, 75, 10)
);


/*
 * INSTANCIAS DE POKEMONES
 */
$charizard = new Pokemon(1, "Charizard", "macho", 500, $tipoFuego, $ataquesFuego, 100);
$charmeleon = new Pokemon(2, "Charmeleon", "macho", 400, $tipoFuego, $ataquesFuego, 100);
$magmar = new Pokemon(3, "Magmar", "macho", 450, $tipoFuego, $ataquesFuego, 100);

$venusaur = new Pokemon(4, "Venusaur", "macho", 500, $tipoHierba, $ataquesHierba, 100);
$ivysaur = new Pokemon(5, "Ivysaur", "macho", 400, $tipoHierba, $ataquesHierba, 100);
$sceptile = new Pokemon(6, "Sceptile", "macho", 450, $tipoHierba, $ataquesHierba, 100);

$feraligatr = new Pokemon(7, "Feraligatr", "macho", 500, $tipoAgua, $ataquesAgua, 100);
$croconaw = new Pokemon(8, "Croconaw", "macho", 400, $tipoAgua, $ataquesAgua, 100);
$greninja = new Pokemon(9, "Greninja", "macho", 450, $tipoAgua, $ataquesAgua, 100);

$gengar = new Pokemon(10, "Gengar", "macho", 450, $tipoFantasma, $ataquesAgua, 100);


/*
 * INSTANCIAS DE TRAINERS
 */
$pkmAndres = [$charizard, $ivysaur, $greninja];
$pkmLaura = [$gengar, $venusaur, $croconaw];
$pkmKevin = [$charmeleon, $venusaur, $gengar];
$pkmAndrea = [$magmar, $sceptile, $croconaw];
$pkmJoel = [$feraligatr, $venusaur, $sceptile];
$pkmSantiago = [$feraligatr, $charizard, $gengar];
$pkmPaola = [$gengar, $venusaur, $greninja];
$pkmPablo = [$feraligatr, $charizard, $ivysaur];
$pkmManuel = [$charizard, $charmeleon, $magmar];
$pkmDavid = [$ivysaur, $venusaur, $sceptile];
$pkmNatalia = [$gengar, $venusaur, $charmeleon];
$pkmNatali = [$feraligatr, $croconaw, $greninja];
$pkmLuisa = [$gengar, $charizard, $greninja];
$pkmXiomara  = [$feraligatr, $venusaur, $sceptile];
$pkmAsh = [$croconaw, $feraligatr, $sceptile];
$pkmPepe = [$gengar, $venusaur, $greninja];



$Andres = TrainerFactory::getTrainer(16,"Andres", 10, 20, "Cali",$pkmAndres);
$Laura = TrainerFactory::getTrainer(1,"Laura", 10, 20, "Paleta",$pkmLaura);
$Kevin = TrainerFactory::getTrainer(2,"Kevin", 10, 20, "Cali",$pkmKevin);
$Andrea = TrainerFactory::getTrainer(3,"Andrea", 10, 20, "Bogota",$pkmAndrea);
$Joel = TrainerFactory::getTrainer(4,"Joel", 10, 20, "Paleta",$pkmJoel);
$Santiago = TrainerFactory::getTrainer(5,"Santiago", 10, 20, "Cali",$pkmSantiago);
$Paola = TrainerFactory::getTrainer(6,"Paola", 10, 20, "Cali",$pkmPaola);
$Pablo = TrainerFactory::getTrainer(7,"Pablo", 10, 20, "Paleta",$pkmPablo); 
$Manuel = TrainerFactory::getTrainer(8,"Manuel", 10, 20, "Cali",$pkmManuel);
$David = TrainerFactory::getTrainer(9,"David", 10, 20, "Paleta",$pkmDavid);
$Natalia = TrainerFactory::getTrainer(10,"Natalia", 10, 20, "Paleta",$pkmNatalia);
$Natali = TrainerFactory::getTrainer(11,"Natali", 10, 20, "Cali",$pkmNatali);
$Luisa = TrainerFactory::getTrainer(12,"Luisa", 10, 20, "Bogota",$pkmLuisa);
$Xiomara = TrainerFactory::getTrainer(13,"Xiomara", 10, 20, "Cali",$pkmXiomara);
$Ash = TrainerFactory::getTrainer(14,"Ash", 10, 20, "Bogota",$pkmAsh);
$Pepe = TrainerFactory::getTrainer(15,"Pepe", 10, 20, "Cali",$pkmPepe);


/*
 * EJEMPLO DE DAÑO
 */

$atacante = $charizard;
$defensor = $venusaur;

function damage($atacante, $defensor) {
    if ($atacante->getTipo()->getTipo() == "Fuego") {

        if ($defensor->getTipo()->getTipo() == "Hierba") {
            $resultado = ($defensor->getHp() - $atacante->getAtaques()[lanzallamas]->getDaño() * 2);
            $defensor->setHp($resultado);
            return $defensor->getHp();
        }
        if ($defensor->getTipo()->getTipo() == "Fuego") {
            $resultado = ($defensor->getHp() - $atacante->getAtaques()[lanzallamas]->getDaño() * 1);
            $defensor->setHp($resultado);
            return $defensor->getHp();
        }
        if ($defensor->getTipo()->getTipo() == "Agua") {
            $resultado = ($defensor->getHp() - $atacante->getAtaques()[lanzallamas]->getDaño() * 1);
            $defensor->setHp($resultado);
            return $defensor->getHp();
        }
        if ($defensor->getTipo()->getTipo() != "Fuego" || $defensor->getTipo()->getTipo() != "Agua" || $defensor->getTipo()->getTipo() != "Hierba") {
            $resultado = ($defensor->getHp() - $atacante->getAtaques()[lanzallamas]->getDaño() * 0);
            $defensor->setHp($resultado);
            return $defensor->getHp();
        }
    }
}

$atacante2 = $charizard;
$defensor2 = $venusaur;
function cantidadAtaques($atacante2, $defensor2) {
    if ($atacante2->getTipo()->getTipo() == "Fuego") {

        if ($defensor2->getTipo()->getTipo() == "Hierba") {        
            $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
            $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
            return $atacante2->getAtaques()[lanzallamas]->getPp();            
        }
        if ($defensor2->getTipo()->getTipo() == "Fuego") {
            $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
            $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
            return $atacante2->getAtaques()[lanzallamas]->getPp(); 
        }
        if ($defensor2->getTipo()->getTipo() == "Agua") {
            $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
            $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
            return $atacante2->getAtaques()[lanzallamas]->getPp(); 
        }
        if ($defensor2->getTipo()->getTipo() != "Fuego" || $defensor2->getTipo()->getTipo() != "Agua" || $defensor2->getTipo()->getTipo() != "Hierba") {
            $cantidadPp = ($atacante2->getAtaques()[lanzallamas]->getPp() - 1);
            $$atacante2->getAtaques([lanzallamas]->setPp($cantidadPp));            
            return $atacante2->getAtaques()[lanzallamas]->getPp(); 
        }
    }
}

echo "<br>";
echo "Hp de " . $defensor->getNombre() . " es: " . $defensor->getHp();
echo "<br>Despues del ataque de: " . $atacante->getNombre();
damage($atacante, $defensor);
echo "<br>Ahora es hp de " . $defensor->getNombre() . " es: " . $defensor->getHp();
cantidadAtaques($atacante2, $defensor2);
echo "<br>Ahora los pp de: " . $atacante->getNombre(). " es: ". $atacante->getAtaques()[lanzallamas]->getPp();
