<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ITrainerFactory
 *
 * @author Kev
 */
interface ITrainerFactory {
    public static function getTrainer($id, $nombre, $medallas, $edad, $pueblo, $pokemons):Trainer;
    public static function getTrainerAmistoso($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \TrainerAmistoso;
    public static function getTrainerAventajado($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \TrainerAventajado;
    public static function getTrainerCompetitivo($id, $nombre, $medallas, $edad, $pueblo, $pokemons): \TrainerCompetitivo;
}
