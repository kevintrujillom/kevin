<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipo
 *
 * @author Kev
 */
class Tipo implements IModel{
    //put your code here
    private $tipo;
    private $fortalezas = [];
    private $debilidades = [];
    
    function __construct($tipo, $fortalezas, $debilidades) {
        $this->tipo = $tipo;
        $this->fortalezas = $fortalezas;
        $this->debilidades = $debilidades;
    }

    public function getMyVars() {
        
    }
    
    function getTipo() {
        return $this->tipo;
    }

    function getFortalezas() {
        return $this->fortalezas;
    }

    function getDebilidades() {
        return $this->debilidades;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setFortalezas($fortalezas) {
        $this->fortalezas = $fortalezas;
    }

    function setDebilidades($debilidades) {
        $this->debilidades = $debilidades;
    }


}
