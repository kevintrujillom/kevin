<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pokemon
 *
 * @author pabhoz
 */
class Pokemon implements IModel {

    //put your code here

    private $id;
    private $nombre;
    private $genero;
    private $hp;
    private $tipo;
    private $ataques = [];
    private $nivel;

    function __construct($id, $nombre, $genero, $hp, $tipo, $ataques, $nivel) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->hp = $hp;
        $this->tipo = $tipo;
        $this->ataques = $ataques;
        $this->nivel = $nivel;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getGenero() {
        return $this->genero;
    }

    function getHp() {
        return $this->hp;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getAtaques() {
        return $this->ataques;
    }

    function getNivel() {
        return $this->nivel;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setHp($hp) {
        $this->hp = $hp;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setAtaques($ataques) {
        $this->ataques = $ataques;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }


}
