<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Batalla
 *
 * @author Kev
 */
class Batalla implements IModel{
    //put your code here
    
    private $trainer1;
    private $trainer2;
    private $ganador;
    
    function __construct($trainer1, $trainer2, $ganador) {
        $this->trainer1 = $trainer1;
        $this->trainer2 = $trainer2;
        $this->ganador = $ganador;
    }

    public function getMyVars() {
        
    }
    
    function getTrainer1() {
        return $this->trainer1;
    }

    function getTrainer2() {
        return $this->trainer2;
    }

    function getGanador() {
        return $this->ganador;
    }

    function setTrainer1($trainer1) {
        $this->trainer1 = $trainer1;
    }

    function setTrainer2($trainer2) {
        $this->trainer2 = $trainer2;
    }

    function setGanador($ganador) {
        $this->ganador = $ganador;
    }


    
    
}
