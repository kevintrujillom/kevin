<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ataque
 *
 * @author Kev
 */
class Ataque implements IModel{
    //put your code here
    
    private $nombre;
    private $tipo;
    private $daño;
    private $pp;
    
    
    function __construct($nombre, $tipo, $daño, $pp) {
        $this->nombre = $nombre;
        $this->tipo = $tipo;
        $this->daño = $daño;
        $this->pp = $pp;
    }

    public function getMyVars() {
        
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getDaño() {
        return $this->daño;
    }

    function getPp() {
        return $this->pp;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setDaño($daño) {
        $this->daño = $daño;
    }

    function setPp($pp) {
        $this->pp = $pp;
    }


}
